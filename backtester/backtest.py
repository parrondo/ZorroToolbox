import re
import os
import sys
import ast

#### DEFAULT VARIABLES ####

zorro_path = '/cygdrive/c/Zorro/'
assets = ["EUR/USD", "GBP/USD", "AUD/USD", "NZD/USD", "USD/CHF", "USD/JPY"]
begin = 2003
end = 2017
directive = ''
strategy = ''

#### METHODS ####

def show_help():
	print 'Usage: python backtest.py [OPTIONS]'
	print '\nList of options:'
	print '-s=STRATEGY\tName of the strategy to run.'
	print '-a=ASSETS\tList of assets for backtesting.'
	print '-b=YEAR\t\tBeginning year.'
	print '-e=YEAR\t\tEnd year.'
	print '-d=DEFINENAME\t#define statement to be passed to Zorro.'

def backtest(zorro_path, strategy, assets, begin, end, directive=''):
	assert os.path.isdir(zorro_path), 'Is not a valid path'
	assert strategy != '', 'Strategy name empty'
	assert isinstance(assets, list), 'Assets have to be a list'
	assert len(assets) > 0, 'There has to be at least one asset'
	assert begin > 1000, 'Invalid begin date'
	assert end > 1000, 'Invalid end date'

	zorro = '{}Zorro.exe {} -run -h -i {} -i {} -a {} -d {}'
	mkdir = 'mkdir -p {}Log/{}/{}/{}'
	mv = 'mv {}Log/{}*.* {}Log/testtrades.csv {}Log/zorro.css {}Log/{}/{}/{}/'
	cp = 'cp {}Strategy/{} {}Log/{}/'
	for asset in assets:
		asset_dir = re.sub('/', '', asset)
		# For tick data split backtests by single year...
		if directive == 'TCK' and begin < end and begin < 9999 and end < 9999:
			for yr in range(begin, end+1):
				zorro_call = zorro.format(zorro_path, strategy, yr, yr, asset,\
					directive)
				mkdir_call = mkdir.format(zorro_path, strategy, asset_dir, yr)
				mv_call = mv.format(zorro_path, strategy, zorro_path,\
					zorro_path, zorro_path, strategy, asset_dir, yr)
				print zorro_call
				os.system(zorro_call)
				os.system(mkdir_call)
				os.system(mv_call)
		# For T6 data backtest the whole range...
		else:
			yr = str(begin)+'-'+str(end)
			zorro_call = zorro.format(zorro_path, strategy, begin, end, asset,\
				directive)
			mkdir_call = mkdir.format(zorro_path, strategy, asset_dir, yr)
			mv_call = mv.format(zorro_path, strategy, zorro_path, zorro_path, \
				zorro_path, strategy, asset_dir, yr)
			print zorro_call
			os.system(zorro_call)
			os.system(mkdir_call)
			os.system(mv_call)

	cp_call = cp.format(zorro_path, strategy+'.c', zorro_path, strategy)
	os.system(cp_call)

# If the file is run as a script:
if __name__ == '__main__':

	#### PARSE COMMAND LINE ARGUMENTS ####

	if len(sys.argv) < 2:
		print('\nWrong number of arguments.')
		show_help()

	for arg in sys.argv:
		try:
			if arg.startswith('-s'):
				strategy = arg[3:]
			elif arg.startswith('-b'):
				begin = int(arg[3:])
			elif arg.startswith('-e'):
				end = int(arg[3:])
			elif arg.startswith('-d'):
				directive = arg[3:]
			elif arg.startswith('-a'):
				assets = [a.strip() for a in arg[3:].split(',')]
			else:
				if arg != 'backtest.py':
					print 'Unrecognized argument: {}'.format(arg)
		except:
			show_help()
			sys.exit(1)

	if strategy == '' or begin < 1000 or end < 1000:
		print "Strategy has to be specified"
		show_help()
		sys.exit(1)
	elif len(assets) == 0:
		print "No assets specified"
		sys.exit(1)

	#### RUN BACKTEST FUNCTION ####

	backtest(zorro_path, strategy, assets, begin, end, directive)

